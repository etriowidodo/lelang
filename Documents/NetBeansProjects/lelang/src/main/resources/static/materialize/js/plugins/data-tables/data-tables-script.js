$(document).ready(function(){
    $('#data-table-simple').DataTable({
            "scrollY":        "350px",
            "scrollX": true,
            "searching": false,
            "bInfo" : false,
            "scrollCollapse": true,
            "paging":         false
        });
     $('#data-table-simple2').DataTable({
            "scrollY":        "350px",
            "searching": false,
            "scrollX": true,
            "bInfo" : false,    
            "scrollCollapse": true,
            "paging":         false
        });
         $('#data-table-simple3').DataTable({
            "scrollY":        "100px",
            "searching": false,
            "scrollX": true,
            "bInfo" : false,    
            "scrollCollapse": true,
            "paging":         false
        });
         $('#data-table-simple4').DataTable({
            "scrollY":        "100px",
            "searching": false,
            "scrollX": true,
            "bInfo" : false,    
            "scrollCollapse": true,
            "paging":         false
        });
          $('#data-table-simple5').DataTable({
            "scrollY":        "100px",
            "searching": false,
            "scrollX": true,
            "bInfo" : false,    
            "scrollCollapse": true,
            "paging":         false
        });
    
    
    var table = $('#data-table-row-grouping').DataTable({
        "columnDefs": [
            { "visible": false, "targets": 2 }
        ],
        "order": [[ 2, 'asc' ]],
        "displayLength": 25,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
    });
 
    // Order by the grouping
    $('#data-table-row-grouping tbody').on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
            table.order( [ 2, 'desc' ] ).draw();
        }
        else {
            table.order( [ 2, 'asc' ] ).draw();
        }
    } );


    });